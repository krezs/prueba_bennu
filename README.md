Proyecto realiazo para la prueba de selección de bennu

- El proyecto fue realizado en Java JSP con base de datos MYSQL, el modelo para la base de datos utilizado es un poco distinto del indicado (el nombre del .sql usado como BDD es "colegio_prueba_bennu.sql asi mismo el nombre de la bdd utilizado fue colegio_prueba_bennu").

- El desarrollo de las consultas SQL que se pidieron estan en ejercicios.sql

Ambos .sql estan dentro de la carpeta sql dentro de web (/web/sql/)

Para el desarrollo del sistema utilice la herramienta NetBeans

Para el diseño de la misma utilice bootstrap 4

Para controlar los datos utilice DAO y Servlets.