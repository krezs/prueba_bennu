package modelo;

import java.util.*;
import java.sql.*;
import uml.Colegio;

public class DAOColegio implements operaciones {

    Database db = new Database();

    @Override
    public boolean insertar(Object obj) {
        Colegio colegio = (Colegio) obj;
        Connection conn;
        PreparedStatement pst;
        String sql = "insert into colegio (nombre,direccion) values (?,?)";
        boolean respuesta = false;
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);
            pst.setString(1, colegio.getNombre());
            pst.setString(2, colegio.getDireccion());
            int filas = pst.executeUpdate();
            respuesta = filas > 0;
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            return false;
        }
        return respuesta;
    }

    @Override
    public boolean eliminar(Object obj) {
        Colegio colegio = (Colegio) obj;
        Connection conn;
        PreparedStatement pst;
        String sql = "delete from colegio where id=?";
        boolean respuesta = false;
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);
            pst.setInt(1, colegio.getId());
            int filas = pst.executeUpdate();
            respuesta = filas > 0;
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            return respuesta;
        }
        return respuesta;
    }

    @Override
    public boolean modificar(Object obj) {
        Colegio colegio = (Colegio) obj;
        Connection conn;
        PreparedStatement pst;
        String sql = "update colegio set nombre=?, direccion=? where id=?";
        boolean respuesta = false;
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);
            pst.setString(1, colegio.getNombre());
            pst.setString(2, colegio.getDireccion());
            pst.setInt(3, colegio.getId());
            int filas = pst.executeUpdate();
            respuesta = filas > 0;
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            return false;
        }
        return respuesta;
    }

    @Override
    public List<Colegio> consultar() {
        List<Colegio> colegios = new ArrayList<>();
        Connection conn;
        PreparedStatement pst;
        ResultSet rs;
        String sql = "select * from colegio";
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                colegios.add(new Colegio(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        rs.getString("direccion")
                ));
            }
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {

        }
        return colegios;
    }

    @Override
    public List<?> filtrar(String campo, String criterio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
