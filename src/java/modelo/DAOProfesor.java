package modelo;

import java.util.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import uml.Profesor;

public class DAOProfesor implements operaciones {

    Database db = new Database();

    @Override
    public boolean insertar(Object obj) {
        Profesor profesor = (Profesor) obj;
        Connection conn;
        PreparedStatement pst;
        String sql = "insert into profesor (nombre,fecha_nacimiento,activo,colegio_id,asignatura) values (?,?,?,?,?)";
        boolean respuesta = false;
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            java.util.Date javaDate = sdf.parse(profesor.getFechaNacimiento());
            java.sql.Date sqlDate = new java.sql.Date(javaDate.getTime());

            pst.setString(1, profesor.getNombre());
            pst.setDate(2, sqlDate);
            pst.setInt(3, profesor.getActivo());
            pst.setInt(4, profesor.getIdColegio());
            pst.setString(5, profesor.getAsignatura());
            int filas = pst.executeUpdate();
            respuesta = filas > 0;
            conn.close();
        } catch (ParseException | ClassNotFoundException | SQLException e) {
            return false;
        }
        return respuesta;
    }

    @Override
    public boolean eliminar(Object obj) {
        Profesor profesor = (Profesor) obj;
        Connection conn;
        PreparedStatement pst;
        String sql = "delete from profesor where id=?";
        boolean respuesta = false;
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);
            pst.setInt(1, profesor.getId());
            int filas = pst.executeUpdate();
            respuesta = filas > 0;
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            return respuesta;
        }
        return respuesta;
    }

    @Override
    public boolean modificar(Object obj) {
        Profesor profesor = (Profesor) obj;
        Connection conn;
        PreparedStatement pst;
        String sql = "update profesor set nombre=?,fecha_nacimiento=?,activo=?,colegio_id=?,asignatura=? where id=?";
        boolean respuesta = false;
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            java.util.Date javaDate = sdf.parse(profesor.getFechaNacimiento());
            java.sql.Date sqlDate = new java.sql.Date(javaDate.getTime());

            pst.setString(1, profesor.getNombre());
            pst.setDate(2, sqlDate);
            pst.setInt(3, profesor.getActivo());
            pst.setInt(4, profesor.getIdColegio());
            pst.setString(5, profesor.getAsignatura());
            pst.setInt(6, profesor.getId());

            int filas = pst.executeUpdate();
            respuesta = filas > 0;
            conn.close();
        } catch (ParseException | ClassNotFoundException | SQLException e) {
            return false;
        }
        return respuesta;
    }

    @Override
    public List<Profesor> consultar() {
        List<Profesor> profesores = new ArrayList<>();
        Connection conn;
        PreparedStatement pst;
        ResultSet rs;
        String sql = "select pf.*,cl.nombre as nombre_colegio from profesor pf join colegio cl on cl.id=pf.colegio_id";
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            while (rs.next()) {
                profesores.add(new Profesor(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        dateFormat.format(rs.getDate("fecha_nacimiento")),
                        rs.getInt("activo"),
                        rs.getInt("colegio_id"),
                        rs.getString("asignatura"),
                        rs.getString("nombre_colegio")
                ));
            }
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {

        }
        return profesores;
    }

    @Override
    public List<Object> filtrar(String campo, String criterio) {
        List<Object> asignatura = new ArrayList<>();
        Connection conn;
        PreparedStatement pst;
        ResultSet rs;
        String sql = "select * from asignatura";
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                asignatura.add(rs.getInt("id"), rs.getString("nombre"));
            }
            conn.close();
        } catch (ClassNotFoundException | SQLException e) {

        }
        return asignatura;
    }

    public Profesor buscar(int id) {
        //List<Object> asignatura = new ArrayList<>();
        Connection conn;
        PreparedStatement pst;
        ResultSet rs;
        String sql = "select * from profesor where id=?";
        Profesor p;
        try {
            Class.forName(db.getDriver());
            conn = DriverManager.getConnection(db.getUrl(), db.getUsuario(), db.getPws());
            pst = conn.prepareStatement(sql);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            if (rs.next()) {
                p = new Profesor(
                        rs.getInt("id"),
                        rs.getString("nombre"),
                        dateFormat.format(rs.getDate("fecha_nacimiento")),
                        rs.getInt("activo"),
                        rs.getInt("colegio_id"),
                        rs.getString("asignatura"),
                        ""
                );
            }else{
                p=null;
            }

            conn.close();

        } catch (ClassNotFoundException | SQLException e) {
            return null;
        }
        return p;

    }
}
