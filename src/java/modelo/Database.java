package modelo;

public class Database {
   private String driver;
   private String url;
   private String usuario;
   private String pws;

    public Database() {
        this.driver = "com.mysql.jdbc.Driver";
        this.url = "jdbc:mysql://localhost:3306/colegio_prueba_bennu";
        this.usuario = "root";
        this.pws = "";
    }

    public String getDriver() {
        return driver;
    }

    public String getUrl() {
        return url;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getPws() {
        return pws;
    }

}
