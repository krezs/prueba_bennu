package modelo;

import java.util.List;

public interface operaciones {
   public boolean insertar(Object obj);
   public boolean eliminar(Object obj);
   public boolean modificar(Object obj);
   public List<?> consultar();
   public List<?> filtrar(String campo, String criterio);
}
