package uml;

import java.util.ArrayList;
import java.util.List;

public class Profesor {

    int id;
    String Nombre;
    String FechaNacimiento;
    int Activo;
    int idColegio;
    String Asignatura;
    String NombreColegio = "";

    public Profesor() {
    }

    public Profesor(int id, String Nombre, String FechaNacimiento, int Activo, int idColegio, String Asignatura, String NombreColegio) {
        this.id = id;
        this.Nombre = Nombre;
        this.FechaNacimiento = FechaNacimiento;
        this.Activo = Activo;
        this.idColegio = idColegio;
        this.Asignatura = Asignatura;
        this.NombreColegio = NombreColegio;
    }

    public String getNombreColegio() {
        return NombreColegio;
    }

    public void setNombreColegio(String NombreColegio) {
        this.NombreColegio = NombreColegio;
    }

    public String getAsignatura() {
        return Asignatura;
    }

    public void setAsignatura(String Asignatura) {
        this.Asignatura = Asignatura;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(String FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }

    public int getActivo() {
        return Activo;
    }

    public void setActivo(int Activo) {
        this.Activo = Activo;
    }

    public int getIdColegio() {
        return idColegio;
    }

    public void setIdColegio(int idColegio) {
        this.idColegio = idColegio;
    }

    public Profesor findId(int id, List<Profesor> al) {
        for (Profesor p : al) {
            if (p.getId() == id) {
            }
            return p;
        }
       return null;
    }
}
