
package uml;

public class Asignatura {
    int Id;
    String nombre;

    public Asignatura(int Id, String nombre) {
        this.Id = Id;
        this.nombre = nombre;
    }

    public Asignatura() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}


