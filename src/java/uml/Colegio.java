package uml;

public class Colegio {
    int id;
    String Nombre;
    String Direccion;

    public Colegio() {
    }

    public Colegio(int id, String Nombre, String Direccion) {
        this.id = id;
        this.Nombre = Nombre;
        this.Direccion = Direccion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }
     
}
