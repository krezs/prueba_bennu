/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAOColegio;
import uml.Colegio;

/**
 *
 * @author Krez
 */
public class ServletColegio extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String boton = request.getParameter("btnAccion");
        if (boton.equals("Insertar")) {
            insertar(request, response);
        } else if (boton.equals("eliminar")) {
            eliminar(request, response);
        } else if (boton.equals("Actualizar")) {
            actualizar(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void insertar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOColegio daoColegio = new DAOColegio();
        Colegio colegio = new Colegio();
        List<Colegio> colegios = new ArrayList<>();
        boolean rsp = false;
        try {

            colegio.setNombre(request.getParameter("txtNombre"));
            colegio.setDireccion(request.getParameter("txtDireccion"));
            rsp = daoColegio.insertar(colegio);
            String mensaje = rsp ? "Agrego Correctamente." : "Ocurrio un error";
            request.setAttribute("rsp", mensaje);

        } catch (Exception e) {
            request.setAttribute("rsp", e.getMessage());
        } finally {
            request.getRequestDispatcher("/colegios.jsp").forward(request, response);
        }
    }

    private void eliminar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOColegio daoColegio = new DAOColegio();
        Colegio colegio = new Colegio();
        boolean rsp = false;
        try {
            colegio.setId(Integer.parseInt(request.getParameter("id")));
            rsp = daoColegio.eliminar(colegio);
            String mensaje = rsp ? "Actualizo Correctamente." : "Ocurrio un error";
            request.setAttribute("rsp", mensaje);
        } catch (Exception e) {
            request.setAttribute("rsp", e.getMessage());
        } finally {
            request.getRequestDispatcher("/colegios.jsp").forward(request, response);
        }
    }

    private void actualizar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOColegio daoColegio = new DAOColegio();
        Colegio colegio = new Colegio();
        boolean rsp = false;
        try {
            colegio.setId(Integer.parseInt(request.getParameter("txtId")));
            colegio.setNombre(request.getParameter("txtNombre"));
            colegio.setDireccion(request.getParameter("txtDireccion"));
            rsp = daoColegio.modificar(colegio);
            String mensaje = rsp ? "Actualizo Correctamente." : "Ocurrio un error";
            request.setAttribute("rsp", mensaje);

        } catch (Exception e) {
            request.setAttribute("rsp", e.getMessage());
        } finally {
            request.getRequestDispatcher("/colegios.jsp").forward(request, response);
        }
    }

}
