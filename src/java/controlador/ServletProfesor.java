/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.DAOProfesor;
import uml.Profesor;

/**
 *
 * @author Krez
 */
public class ServletProfesor extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String boton = request.getParameter("btnAccion");
        if (boton.equals("Insertar")) {
            insertar(request, response);
        } else if (boton.equals("eliminar")) {
            eliminar(request, response);
        } else if (boton.equals("Actualizar")) {
            actualizar(request, response);
        } else if (boton.equals("buscar")) {
            buscar(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void insertar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOProfesor daoProfesor = new DAOProfesor();
        Profesor profesor = new Profesor();
        List<Profesor> profesores = new ArrayList<>();
        boolean rsp = false;
        try {

            profesor.setNombre(request.getParameter("txtNombre"));
            profesor.setFechaNacimiento(request.getParameter("txtFechaNacimiento"));
            if (request.getParameter("chkActivo") != null) {
                profesor.setActivo(1);
            } else {
                profesor.setActivo(0);
            }
            profesor.setIdColegio(Integer.parseInt(request.getParameter("txtColegio")));
            profesor.setAsignatura(request.getParameter("cboAsignatura"));
            profesor.setNombreColegio("");
            rsp = daoProfesor.insertar(profesor);

            String mensaje = rsp ? "Agrego Correctamente." : "Ocurrio un error";
            request.setAttribute("rsp", mensaje);

        } catch (Exception e) {
            request.setAttribute("rsp", e.getMessage());
        } finally {
            request.getRequestDispatcher("/profesores.jsp").forward(request, response);
        }
    }

    private void eliminar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOProfesor daoProfesor = new DAOProfesor();
        Profesor profesor = new Profesor();
        boolean rsp = false;
        try {
            profesor.setId(Integer.parseInt(request.getParameter("id")));
            rsp = daoProfesor.eliminar(profesor);
            String mensaje = rsp ? "Elimino Correctamente." : request.getParameter("id");
            request.setAttribute("rsp", mensaje);

        } catch (Exception e) {
            request.setAttribute("rsp", e.getMessage());
        } finally {
            request.getRequestDispatcher("/profesores.jsp").forward(request, response);
        }
    }

    private void actualizar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOProfesor daoProfesor = new DAOProfesor();
        Profesor profesor = new Profesor();
        boolean rsp = false;
        try {
            profesor.setId(Integer.parseInt(request.getParameter("txtId")));
            profesor.setNombre(request.getParameter("txtNombre"));
            profesor.setFechaNacimiento(request.getParameter("txtFechaNacimiento"));
            if (request.getParameter("chkActivo") != null) {
                profesor.setActivo(1);
            } else {
                profesor.setActivo(0);
            }
            profesor.setIdColegio(Integer.parseInt(request.getParameter("txtColegio")));
            profesor.setAsignatura(request.getParameter("cboAsignatura"));
            profesor.setNombreColegio("");

            rsp = daoProfesor.modificar(profesor);
            String mensaje = rsp ? "Actualizo Correctamente." : "Ocurrio un error";
            request.setAttribute("rsp", mensaje);

        } catch (Exception e) {
            request.setAttribute("rsp", e.getMessage());
        } finally {
            request.getRequestDispatcher("/profesores.jsp").forward(request, response);
        }
    }

    private void buscar(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            DAOProfesor daoProfesor = new DAOProfesor();
            Profesor profesor = daoProfesor.buscar((Integer.parseInt(request.getParameter("id"))));
            String jsonMentirita = "{ \"id\": \"" + profesor.getId() + "\", \"nombre\": \"" + profesor.getNombre() + "\", \"fechaNacimiento\": \"" + profesor.getFechaNacimiento() + "\" , \"activo\": \"" + profesor.getActivo() + "\", \"cid\": \"" + profesor.getIdColegio() + "\", \"asignatura\": \"" + profesor.getAsignatura() + "\"}";
            out.print(jsonMentirita);
            out.flush();
        } catch (NumberFormatException e) {
            out.println("Error: " + e.getMessage());
        }

        /**
         * response.setContentType("text/html;charset=UTF-8");
         * response.setContentType("application/json");
         * response.setCharacterEncoding("UTF-8"); PrintWriter out =
         * response.getWriter(); out.print(profesor.getNombre())
         *
         */
//out.println("llego el ID "+ id);
    }

}
