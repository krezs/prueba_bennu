<%@page import="java.util.stream.Collectors"%>
<%@page import="modelo.DAOAsignatura"%>
<%@page import="uml.Asignatura"%>
<%@page import="uml.Colegio"%>
<%@page import="modelo.DAOColegio"%>
<%@page import="modelo.DAOProfesor" %>
<%@page import="java.util.*" %>
<%@page import="uml.Profesor" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Controlador Profesores</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap-datepicker.css" rel="stylesheet" type="text/css"/>
    </head>
    <%
        DAOProfesor daoProfesores = new DAOProfesor();
        List<Profesor> profesores = new ArrayList();
        DAOColegio daoColegio = new DAOColegio();
        List<Colegio> colegios = new ArrayList();
        DAOAsignatura daoAsignatura = new DAOAsignatura();
        List<Asignatura> asignaturas = new ArrayList();
    %>
    <jsp:include page="menu.jsp"></jsp:include>
        <body>

            <div class="container mt-5 mb-5">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Listado de colegios</h3>
                        <table class="table table-striped mt-5">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Colegio</th>
                                    <th scope="col">Asignatura</th>
                                    <th scope="col">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            <%
                                colegios = daoColegio.consultar();
                                profesores = daoProfesores.consultar();
                                asignaturas = daoAsignatura.consultar();
                                for (Profesor profesor : profesores) {
                            %>
                            <tr>
                                <th scope="row" class="nro"><%= profesor.getId()%></th>
                                <td class="nom"><%= profesor.getNombre()%></td>
                                <td class="cole"><%= profesor.getNombreColegio()%></td>
                                <td class="asg"><%= profesor.getAsignatura()%></td>
                                <td> 
                                    <button type="button" class="btn btn-info " id="<%= profesor.getId()%>" onclick="editar(this)" >Editar</button>
                                    <a href="ServletProfesor?btnAccion=eliminar&id=<%= profesor.getId()%>" type="button" class="btn btn-danger" id="btnEliminar" name="btnAccion" value="eliminar">Eliminar</a>
                                </td>

                            </tr>
                            <% }%>
                        </tbody>
                    </table>

                </div> 
                <div class="col-md-12">
                    <button id="agregarColegio" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#exampleModal" data-whatever="@cole" >Agregar Profesor</button>

                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Agregar Profesor</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="limpiarCampos()">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="frmProfesor" method="POST" action="ServletProfesor" onsubmit="return validarCampos()">
                                    <input type="hidden" class="form-control" id="txtId" name="txtId">
                                    <div class="form-group">
                                        <label for="txtNombre">Nombre</label>
                                        <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre del profesor" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtFechaNacimiento">Fecha Nacimiento</label>
                                        <div  id="fecha-container"><input type="text" class="form-control" id="txtFechaNacimiento" name="txtFechaNacimiento"  placeholder="01/01/1993" required></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Colegio</label>
                                        <select class="form-control" id="txtColegio" name="txtColegio">
                                            <option value="">Seleccione</option>
                                            <% for (Colegio colegio : colegios) {%>
                                            <option value="<%= colegio.getId()%>"><%= colegio.getNombre()%></option>
                                            <% }%>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Asignatura</label>
                                        <select class="form-control" id="cboAsignatura" name="cboAsignatura">
                                            <option value="">Seleccione</option>
                                            <% for (Asignatura asignatura : asignaturas) {%>
                                            <option value="<%= asignatura.getNombre()%>"><%= asignatura.getNombre()%></option>
                                            <% }%>
                                        </select>
                                    </div>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="chkActivo" name="chkActivo">
                                        <label class="form-check-label" for="exampleCheck1">Activo</label>
                                    </div>
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-block" id="btnAccion" name="btnAccion" value="Insertar">Guardar</button>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="limpiarCampos()">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/swal.js" type="text/javascript"></script>
        <script src="js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script>
                                    $('#fecha-container input').datepicker({
                                        format: "yyyy/mm/dd",
                                        language: "es"
                                    });
        </script>
        <script>
            var rsp = "${rsp}";
            if (rsp != "") {
                swal(
                        '¡Bien!',
                        rsp,
                        'success'
                        );
            }

            function validarCampos() {
                var nm = $("#txtNombre");
                var fc = $("#txtFechaNacimiento");
                var cl = $("#txtColegio");
                var as = $("#cboAsignatura");

                if ((nm.val() != null && nm.val() != '') &&
                        (fc.val() != null && fc.val() != '') &&
                        (cl.val() != null && cl.val() != '') &&
                        (as.val() != null && as.val() != '')) {
                    return true;
                } else {
                    swal({
                        type: 'error',
                        title: 'Ops...',
                        text: 'Porfavor complete todos los campos'
                    });
                    return false;
                }
            }
            function editar(val) {
                var id = $(val).closest("tr").find(".nro").text();

                $.ajax({
                    url: "ServletProfesor",
                    data: {id: id, btnAccion: "buscar"},
                    method: "post",
                    success: function (data) {
                        var profesor = data;
                        $("#txtId").val(profesor.id);
                        $("#txtNombre").val(profesor.nombre);
                        $("#txtFechaNacimiento").val(profesor.fechaNacimiento);
                        $("#txtColegio").val(profesor.cid);
                        $("#cboAsignatura").val(profesor.asignatura);
                        if (profesor.activo == 1) {
                            console.log("activo");
                            $('#chkActivo').attr('checked', true);
                        } else {
                            console.log("inactivo");
                            $('#chkActivo').attr('checked', false);
                        }
                    }});
                var nom = $(val).closest("tr").find(".nom").text();
                var dir = $(val).closest("tr").find(".dir").text();
                $("#btnAccion").val("Actualizar");
                $("#btnAccion").html("Actualizar Profesor");
                $("#exampleModalLabel").html('Actualizar Profesor');
                $('#exampleModal').modal('show');
            }
            $("#agregarColegio").click(function () {
                $("#exampleModalLabel").html('Agregar Profesor');
                $("#txtId").val("");
                $("#txtNombre").val("");
                $("#txtFechaNacimiento").val("");
                $("#txtColegio").val("");
                $("#cboAsignatura").val("");
                $('#chkActivo').attr('checked', false);
                $("#btnAccion").html("Guardar");
                $("#btnAccion").val("Insertar");
            });
            function limpiarCampos() {
                $("#txtId").val("");
                $("#txtNombre").val("");
                $("#txtDireccion").val("");

            }
        </script>
    </body>
</html>
