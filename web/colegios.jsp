<%@page import="modelo.DAOColegio" %>
<%@page import="java.util.*" %>
<%@page import="uml.Colegio" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Controlador Colegios</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    </head>
    <%
        DAOColegio daoColegio = new DAOColegio();
        List<Colegio> colegios = new ArrayList();
    %>
    <jsp:include page="menu.jsp"></jsp:include>
        <body>

            <div class="container mt-5">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Listado de colegios</h3>
                        <table class="table table-striped mt-5">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Dirección</th>
                                    <th scope="col">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                            <%
                                colegios = daoColegio.consultar();
                                for (Colegio colegio : colegios) {
                            %>
                            <tr>
                                <th scope="row" class="nro"><%= colegio.getId()%></th>
                                <td class="nom"><%= colegio.getNombre()%></td>
                                <td class="dir"><%= colegio.getDireccion()%></td>
                                <td> 
                                    <button type="button" class="btn btn-info " id="<%= colegio.getId()%>" onclick="editar(this)" >Editar</button>
                                    <a href="ServletColegio?btnAccion=eliminar&id=<%= colegio.getId()%>" type="button" class="btn btn-danger" id="btnEliminar" name="btnAccion" value="eliminar">Eliminar</a>
                                </td>

                            </tr>
                            <% }%>
                        </tbody>
                    </table>

                </div> 
                <div class="col-md-12">
                    <button id="agregarColegio" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#exampleModal" data-whatever="@cole" >Agregar nuevo colegio</button>

                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Agregar Colegio</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="limpiarCampos()">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="frmColegio" method="POST" action="ServletColegio" onsubmit="return validarCampos()">
                                    <input type="hidden" class="form-control" id="txtId" name="txtId">
                                    <div class="form-group">
                                        <label for="txtNombre">Nombre</label>
                                        <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre del colegio" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtDireccion">Dirección</label>
                                        <input type="text" class="form-control" id="txtDireccion" name="txtDireccion" placeholder="Dirección del colegio" required>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block" id="btnAccion" name="btnAccion" value="Insertar">Guardar</button>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="limpiarCampos()">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <script src="js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/swal.js" type="text/javascript"></script>
        <script>
                                        var rsp = "${rsp}";
                                        if (rsp != "") {
                                            swal(
                                                    '¡Bien!',
                                                    rsp,
                                                    'success'
                                                    );
                                        }

                                        function validarCampos() {
                                            var nm = $("#txtNombre");
                                            var dir = $("#txtDireccion");
                                            if ((nm.val() != null && nm.val() != '') && (dir.val() != null && dir.val() != '')) {
                                                return true;
                                            } else {
                                                swal({
                                                    type: 'error',
                                                    title: 'Ops...',
                                                    text: 'Porfavor complete todos los campos'
                                                });
                                                return false;
                                            }
                                        }
                                        function editar(val) {
                                            var id = $(val).closest("tr").find(".nro").text();
                                            var nom = $(val).closest("tr").find(".nom").text();
                                            var dir = $(val).closest("tr").find(".dir").text();
                                            $("#btnAccion").val("Actualizar");
                                            $("#btnAccion").html("Actualizar Colegio");
                                            $("#txtNombre").val(nom);
                                            $("#txtDireccion").val(dir);
                                            $("#txtId").val(id);
                                            $("#exampleModalLabel").html('Actualizar Colegio');
                                            $('#exampleModal').modal('show');
                                        }
                                        $("#agregarColegio").click(function () {
                                            $("#exampleModalLabel").html('Agregar Colegio');
                                            $("#txtId").val("");
                                            $("#btnAccion").html("Guardar");
                                            $("#btnAccion").val("Insertar");
                                        });
                                        function limpiarCampos() {
                                            $("#txtId").val("");
                                            $("#txtNombre").val("");
                                            $("#txtDireccion").val("");

                                        }
        </script>
    </body>
</html>
