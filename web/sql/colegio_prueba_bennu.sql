/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : colegio_prueba_bennu

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-11-11 00:01:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `alumno`
-- ----------------------------
DROP TABLE IF EXISTS `alumno`;
CREATE TABLE `alumno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `colegio_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_alumno_colegio1_idx` (`colegio_id`),
  CONSTRAINT `fk_alumno_colegio1` FOREIGN KEY (`colegio_id`) REFERENCES `colegio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alumno
-- ----------------------------
INSERT INTO `alumno` VALUES ('3', 'Andres', '1995-07-05', '1');
INSERT INTO `alumno` VALUES ('4', 'Matias', '1993-05-11', '1');
INSERT INTO `alumno` VALUES ('5', 'Javiera', '1992-01-01', '1');
INSERT INTO `alumno` VALUES ('6', 'Claudia', '2000-06-14', '1');
INSERT INTO `alumno` VALUES ('7', 'Jacinta', '1989-04-01', '1');

-- ----------------------------
-- Table structure for `asignatura`
-- ----------------------------
DROP TABLE IF EXISTS `asignatura`;
CREATE TABLE `asignatura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of asignatura
-- ----------------------------
INSERT INTO `asignatura` VALUES ('1', 'Matematicas');
INSERT INTO `asignatura` VALUES ('2', 'Lenguaje');
INSERT INTO `asignatura` VALUES ('3', 'Artes Plasticas');
INSERT INTO `asignatura` VALUES ('4', 'Musica');
INSERT INTO `asignatura` VALUES ('5', 'Ingles');

-- ----------------------------
-- Table structure for `colegio`
-- ----------------------------
DROP TABLE IF EXISTS `colegio`;
CREATE TABLE `colegio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) NOT NULL,
  `direccion` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of colegio
-- ----------------------------
INSERT INTO `colegio` VALUES ('1', 'Principado de Asturias', 'Av. Concha y Toro #5555');
INSERT INTO `colegio` VALUES ('37', 'Colegio 37', 'Direccion muy bakan');

-- ----------------------------
-- Table structure for `nota`
-- ----------------------------
DROP TABLE IF EXISTS `nota`;
CREATE TABLE `nota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` float NOT NULL,
  `asignatura_alumno_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_nota_asignatura_alumno1_idx` (`asignatura_alumno_id`),
  CONSTRAINT `fk_nota_asignatura_alumno1` FOREIGN KEY (`asignatura_alumno_id`) REFERENCES `profesor_asignatura_alumno` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nota
-- ----------------------------
INSERT INTO `nota` VALUES ('1', '1.4', '1');
INSERT INTO `nota` VALUES ('2', '2.9', '2');
INSERT INTO `nota` VALUES ('3', '3.2', '3');
INSERT INTO `nota` VALUES ('4', '1.7', '4');
INSERT INTO `nota` VALUES ('5', '6.4', '5');
INSERT INTO `nota` VALUES ('6', '1.9', '6');
INSERT INTO `nota` VALUES ('7', '2.7', '7');
INSERT INTO `nota` VALUES ('8', '5.7', '8');
INSERT INTO `nota` VALUES ('9', '6.7', '9');
INSERT INTO `nota` VALUES ('10', '1.5', '10');
INSERT INTO `nota` VALUES ('22', '4.5', '1');
INSERT INTO `nota` VALUES ('23', '5.9', '2');
INSERT INTO `nota` VALUES ('24', '2.3', '3');
INSERT INTO `nota` VALUES ('25', '2.7', '4');
INSERT INTO `nota` VALUES ('26', '1.5', '5');
INSERT INTO `nota` VALUES ('27', '2.7', '6');
INSERT INTO `nota` VALUES ('28', '3.1', '7');
INSERT INTO `nota` VALUES ('29', '3.7', '8');
INSERT INTO `nota` VALUES ('30', '4.5', '9');
INSERT INTO `nota` VALUES ('31', '5.3', '10');

-- ----------------------------
-- Table structure for `profesor`
-- ----------------------------
DROP TABLE IF EXISTS `profesor`;
CREATE TABLE `profesor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `activo` int(11) NOT NULL,
  `colegio_id` int(11) NOT NULL,
  `asignatura` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profesor_colegio_idx` (`colegio_id`),
  CONSTRAINT `fk_profesor_colegio` FOREIGN KEY (`colegio_id`) REFERENCES `colegio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profesor
-- ----------------------------
INSERT INTO `profesor` VALUES ('1', 'Edwin', '1975-07-17', '1', '1', 'Matematicas');
INSERT INTO `profesor` VALUES ('3', 'Freddy', '1979-06-13', '1', '1', 'Lenguaje');
INSERT INTO `profesor` VALUES ('4', 'Susana', '1990-01-09', '0', '1', 'Musica');
INSERT INTO `profesor` VALUES ('5', 'Andres', '1985-05-04', '1', '1', 'Artes Plasticas');
INSERT INTO `profesor` VALUES ('6', 'Matias Andres', '0006-07-10', '1', '37', 'Matematicas');

-- ----------------------------
-- Table structure for `profesor_asignatura_alumno`
-- ----------------------------
DROP TABLE IF EXISTS `profesor_asignatura_alumno`;
CREATE TABLE `profesor_asignatura_alumno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alumno_id` int(11) NOT NULL,
  `profesor_id` int(11) NOT NULL,
  `asignatura_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_asignatura_alumno_alumno1_idx` (`alumno_id`),
  KEY `fk_profesor_asignatura_alumno_profesor1_idx` (`profesor_id`),
  KEY `fk_profesor_asignatura_alumno_asignatura1_idx` (`asignatura_id`),
  CONSTRAINT `fk_asignatura_alumno_alumno1` FOREIGN KEY (`alumno_id`) REFERENCES `alumno` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profesor_asignatura_alumno_asignatura1` FOREIGN KEY (`asignatura_id`) REFERENCES `asignatura` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profesor_asignatura_alumno_profesor1` FOREIGN KEY (`profesor_id`) REFERENCES `profesor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of profesor_asignatura_alumno
-- ----------------------------
INSERT INTO `profesor_asignatura_alumno` VALUES ('1', '3', '1', '1');
INSERT INTO `profesor_asignatura_alumno` VALUES ('2', '4', '1', '1');
INSERT INTO `profesor_asignatura_alumno` VALUES ('3', '5', '1', '1');
INSERT INTO `profesor_asignatura_alumno` VALUES ('4', '6', '1', '1');
INSERT INTO `profesor_asignatura_alumno` VALUES ('5', '7', '1', '1');
INSERT INTO `profesor_asignatura_alumno` VALUES ('6', '3', '3', '2');
INSERT INTO `profesor_asignatura_alumno` VALUES ('7', '4', '3', '2');
INSERT INTO `profesor_asignatura_alumno` VALUES ('8', '5', '3', '2');
INSERT INTO `profesor_asignatura_alumno` VALUES ('9', '6', '3', '2');
INSERT INTO `profesor_asignatura_alumno` VALUES ('10', '7', '3', '2');
