-- MySQL Workbench Synchronization
-- Generated: 2018-11-08 20:16
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Krez

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `colegio_prueba_bennu`.`colegio` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(250) NOT NULL,
  `direccion` VARCHAR(250) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `colegio_prueba_bennu`.`asignatura` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `colegio_prueba_bennu`.`profesor` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(200) NOT NULL,
  `fecha_nacimiento` DATETIME NOT NULL,
  `activo` INT(11) NOT NULL,
  `colegio_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_profesor_colegio_idx` (`colegio_id` ASC) ,
  CONSTRAINT `fk_profesor_colegio`
    FOREIGN KEY (`colegio_id`)
    REFERENCES `colegio_prueba_bennu`.`colegio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `colegio_prueba_bennu`.`alumno` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(250) NOT NULL,
  `fecha_nacimiento` DATETIME NOT NULL,
  `colegio_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_alumno_colegio1_idx` (`colegio_id` ASC) ,
  CONSTRAINT `fk_alumno_colegio1`
    FOREIGN KEY (`colegio_id`)
    REFERENCES `colegio_prueba_bennu`.`colegio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `colegio_prueba_bennu`.`nota` (
  `id` INT(11) NOT NULL,
  `nota` FLOAT(11) NOT NULL,
  `asignatura_alumno_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_nota_asignatura_alumno1_idx` (`asignatura_alumno_id` ASC) ,
  CONSTRAINT `fk_nota_asignatura_alumno1`
    FOREIGN KEY (`asignatura_alumno_id`)
    REFERENCES `colegio_prueba_bennu`.`profesor_asignatura_alumno` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `colegio_prueba_bennu`.`profesor_asignatura_alumno` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `alumno_id` INT(11) NOT NULL,
  `profesor_id` INT(11) NOT NULL,
  `asignatura_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_asignatura_alumno_alumno1_idx` (`alumno_id` ASC) ,
  INDEX `fk_profesor_asignatura_alumno_profesor1_idx` (`profesor_id` ASC) ,
  INDEX `fk_profesor_asignatura_alumno_asignatura1_idx` (`asignatura_id` ASC) ,
  CONSTRAINT `fk_asignatura_alumno_alumno1`
    FOREIGN KEY (`alumno_id`)
    REFERENCES `colegio_prueba_bennu`.`alumno` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_profesor_asignatura_alumno_profesor1`
    FOREIGN KEY (`profesor_id`)
    REFERENCES `colegio_prueba_bennu`.`profesor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_profesor_asignatura_alumno_asignatura1`
    FOREIGN KEY (`asignatura_id`)
    REFERENCES `colegio_prueba_bennu`.`asignatura` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
