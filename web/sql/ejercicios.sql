#PRIMERA CONSULTA (SE RETORNA TODOS LOS PROFESORES DEL COLEGIO DE ID 1)
select * from profesor pf where pf.colegio_id=1 order by pf.nombre ASC ;

#SEGUNDA CONSULTA (TODOS LOS ALUMNOS CON PROMEDIO ROJO)
select al.nombre,asg.nombre as nombre_asignatura,avg(round(nt.nota,1)) as promedio from nota nt
join profesor_asignatura_alumno paa on paa.id=nt.asignatura_alumno_id
join alumno al on al.id=paa.alumno_id
join asignatura asg on asg.id=paa.asignatura_id
group by al.id,asg.id
having (avg(nt.nota))<4.0
order by asg.nombre;

#TERCERA CONSULTA (NOTA MAS ALTA, considerar que fue realizado con el modelo que modifique el cual es un poco distinto al enviado.)
select paa.asignatura_id,grupoNota.alumno_id,grupoNota.notaMaxima from nota nt
inner join profesor_asignatura_alumno paa on paa.id=nt.asignatura_alumno_id
inner join (
	select  paa.alumno_id,max(nt.nota) as notaMaxima from nota nt
	inner join profesor_asignatura_alumno paa on paa.id=nt.asignatura_alumno_id
	group by paa.alumno_id
) grupoNota 
on paa.alumno_id=grupoNota.alumno_id
and nt.nota=grupoNota.notaMaxima
inner join (
	select  paa.asignatura_id,max(nt.nota) as notaMaxima2 from nota nt
	inner join profesor_asignatura_alumno paa on paa.id=nt.asignatura_alumno_id
	group by paa.asignatura_id
) grupoNota2
on paa.asignatura_id=grupoNota2.asignatura_id
and nt.nota=grupoNota2.notaMaxima2;







